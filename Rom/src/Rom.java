import java.util.Scanner;
/** LS05.1: Fahrkartenautomat - A4.1: Einführung Auswahlstrukturen
 * AB Fallunterscheidungen 
 * Aufgabe 3: Römische Zahlen
 * 
 * @author Simon Asmus
 * @version 1.6 2021
 *
 */
public class Rom {
    
    public static void main(String[] args) {

	Scanner eingabe = new Scanner(System.in);
	System.out.print(" Bitte geben Sie ein einzelnes römisches Zahlzeichen ein:");
	char zahl = eingabe.next().charAt(0);
	System.out.println();

	switch (zahl) {
		case 'I':
		    System.out.println(" " + zahl + " = " + 1);
		    break;
		case 'V':
		    System.out.println(" " + zahl + " = " + 5);
		    break;
		case 'X':
		    System.out.println(" " + zahl + " = " + 10);
		    break;
		case 'L':
		    System.out.println(" " + zahl + " = " + 50);
		    break;
		case 'C':
		    System.out.println(" " + zahl + " = " + 100);
		    break;
		case 'D':
		    System.out.println(" " + zahl + " = " + 500);
		    break;
		case 'M':
		    System.out.println(" " + zahl + " = " + 1000);
		    break;
		default:
		    System.out.println(" Eingabe ungültig. Bitte geben Sie ein einzelnes römisches Zahlzeichen ein.");
		    break;	
	}
	
    }

}
