/**
 * A1.6: AB "Ausgabeformatierung 2" Aufgabe 3: Temperaturtabelle
 * 
 * @author Simon Asmus
 * @version 1.4 2021
 */
public class Temperaturtabelle {

    public static void main(String[] args) {
	String s1 = "Fahrenheit";
	String s2 = "Celsius";
	String s3 = "-----------------------";
	int i1 = -20;
	int i2 = -10;
	int i3 = 0;
	int i4 = 20;
	int i5 = 30;
	double d1 = -28.8889;
	double d2 = -23.3333;
	double d3 = -17.7778;
	double d4 = -6.6667;
	double d5 = -1.1111;
	System.out.printf("%-11s|", s1);
	System.out.printf("%10s\n", s2);
	System.out.printf("%22s\n", s3);
	System.out.printf("%-11d|", i1);
	System.out.printf("%10.2f\n", d1);
	System.out.printf("%-11d|", i2);
	System.out.printf("%10.2f\n", d2);
	System.out.printf("%+-11d|", i3);
	System.out.printf("%10.2f\n", d3);
	System.out.printf("%+-11d|", i4);
	System.out.printf("%10.2f\n", d4);
	System.out.printf("%+-11d|", i5);
	System.out.printf("%10.2f\n", d5);

    }

}
