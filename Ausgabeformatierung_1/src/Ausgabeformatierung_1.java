/**
 * A1.5: AB "Ausgabeformatierung 1"
 * 
 * @author Simon Asmus
 * @version 1.6 2021
 */
public class Ausgabeformatierung_1 {

    public static void main(String[] args) {

	// Aufgabe 1
	System.out.print("Text hier einfügen.");
	System.out.println(" Fügen Sie Text hier ein.");
	System.out.println();
	System.out.print("\"Text hier einfügen.\"\n" + "\"Fügen Sie Text hier ein.\"\n");
	// Bei der print()-Anweisung würden nachfolgende Ausgaben in der selben Zeile
	// stehen
	// bei der println()-Anweisung wird nach der Ausgabe ein Zeilenumbruch
	// eingefügt,
	// nachfolgende Ausgaben würden in der nächsten Zeile stehen

	// Aufgabe 2
	System.out.println();
	String s = "*************";
	System.out.printf("\n\t      %.1s\t\t\t", s);
	System.out.printf("\n\t     %.3s\t\t\t", s);
	System.out.printf("\n\t    %.5s\t\t\t", s);
	System.out.printf("\n\t   %.7s\t\t\t", s);
	System.out.printf("\n\t  %.9s\t\t\t", s);
	System.out.printf("\n\t %.11s\t\t\t", s);
	System.out.printf("\n\t%.13s\t\t\t", s);
	System.out.printf("\n\t     %.3s\t\t\t", s);
	System.out.printf("\n\t     %.3s\t\t\t", s);
	System.out.println();
	
	//Aufgabe 3
	System.out.println();
	double d1 = 22.4234234;
	double d2 = 111.2222;
	double d3 = 4.0;
	double d4 = 1000000.551;
	double d5 = 97.34;
	System.out.printf("%.2f\n", d1);
	System.out.printf("%.2f\n", d2);
	System.out.printf("%.2f\n", d3);
	System.out.printf("%.2f\n", d4);
	System.out.printf("%.2f\n", d5);
		
    }

}
