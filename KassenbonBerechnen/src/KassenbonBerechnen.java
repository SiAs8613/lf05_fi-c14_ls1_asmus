import java.util.Scanner;
/**LF05 LS05.1 Fahrkartenautomat - A3.2 Methoden verwenden üben
 * 
 * Aufgabe 2
 * 
 * @author Simon Asmus
 * @version 2.1
 */
public class KassenbonBerechnen {

    public static void main(String[] args) {
	
	Scanner myScanner = new Scanner(System.in);

	System.out.println("Was möchten Sie bestellen?");
	String artikel = liesString(myScanner.next());
		
	System.out.println("Geben Sie die Anzahl ein:");
	int anzahl = liesInt(myScanner.nextInt());

	System.out.println("Geben Sie den Nettopreis ein:");
	double preis = liesDouble(myScanner.nextDouble());

	System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
	double mwst = liesDouble(myScanner.nextDouble());

	double nettoGesamtpreis = berechneGesamtnettopreis(anzahl, preis);
	double bruttoGesamtpreis = berechneGesamtbruttopreis(nettoGesamtpreis, mwst);
	
	rechnungAusgeben(artikel, anzahl, nettoGesamtpreis, bruttoGesamtpreis, mwst);

    }
    
    /*
     * 
     */
    public static String liesString(String text) {
	String art = text;
	return art;
    }
    
    /*
     * 
     */
    public static int liesInt(int i) {
	int anz = i;
	return anz;
    }
    
    /*
     * 
     */
    public static double liesDouble(double d) {
	double z = d;
	return z;
    }
    
    /*
     * 
     */
    public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
	double gesNetp = anzahl * nettopreis;
	return gesNetp;
    }
    
    /*
     * 
     */
    public static double berechneGesamtbruttopreis(double nettoGesamtpreis, double mwst) {
	double gesBrutp = nettoGesamtpreis * (1 + mwst / 100);
	return gesBrutp;
    }
    
    /*
     * 
     */
    public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
	System.out.println("\tRechnung");
	System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
    }

}
