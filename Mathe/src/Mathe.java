/**LF05 LS05.1 Fahrkartenautomat - A3.2 Methoden verwenden üben
 * 
 * Aufgaben 5 und 7
 * 
 * @author Simon Asmus
 * @version 1.4
 */
public class Mathe {

    public static void main(String[] args) {
	
	//5. Aufgabe
	double w = 14;
	double ergebnis= quadrat(w);
	System.out.println("5. Aufgabe:");
      	System.out.printf("Das Quadrat von %.2f ist %.2f\n", w, ergebnis);
      	System.out.println("---------------------------------------------");
      	System.out.println("");
      	
      	//7. Aufgabe
      	double k1 = 9;
      	double k2 = 12;
      	double hyp = hypothenuse(k1, k2);
      	System.out.println("7. Aufgabe:");
        System.out.printf("Die Länge der Hypothenuse des rechtwinkligen Dreiecks aus den Katheten %.2f und %.2f ist %.2f\n", k1, k2, hyp);
        System.out.println("---------------------------------------------");
        System.out.println("");
    }
    
    /*Eine Funktion, welche das Quadrat einer Zahl berechnet
     * -> 4. Aufgabe
     */
    public static double quadrat(double x) {
	double y = x * x;
	return y;
    }
    
    /*Eine Funktion, welche die Länge der Hypothenuse eines rechtwinkligen Dreiecks berechnet
     * 
     */
    public static double hypothenuse(double kathete1, double kathete2) {
	double h;
	h = Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
	return h;
    }

}
