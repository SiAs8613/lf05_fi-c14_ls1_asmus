/**LF05 LS05.1 Fahrkartenautomat - A3.2 Methoden verwenden üben
 * 
 * Übungsaufgaben zum Verwenden von Methoden
 * 
 * @author Simon Asmus
 * @version 1.9
 */
public class MethodenUebungen {

    public static void main(String[] args) {
	
	
	//1. Aufgabe
	double x = 2.0;
	double y = 4.0;
	double m = berechneMittelwert(x, y);
	System.out.println("1. Aufgabe:");
      	System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      	System.out.println("---------------------------------------------");
      	System.out.println("");
	
	//3. Aufgabe
	double r01 = 12;
	double r02 = 20;
	double r0102 = reihenschaltung(r01, r02);
	System.out.println("3. Aufgabe:");
      	System.out.printf("Der Ersatzwiderstand der Reihenschaltung von %.2f und %.2f ist %.2f\n", r01, r02, r0102);
      	System.out.println("---------------------------------------------");
      	System.out.println("");
      	
      	//4 Aufgabe
      	double r03 = 1;
      	double r04 = 2;
      	double r0304 = parallelschaltung(r03, r04);
      	System.out.println("4. Aufgabe:");
      	System.out.printf("Der Ersatzwiderstand der Parallelschaltung von %.2f und %.2f ist %.2f\n", r03, r04, r0304);
      	System.out.println("---------------------------------------------");
      	System.out.println("");
    }
    
    /*Eine Funktion, welche den Mittelwert berechnet
     * -> 1. Aufgabe
     * 
     */
    public static double berechneMittelwert(double w, double z) {
	double n = (w + z) / 2.0;
	return n;
    }
    
    /*Eine Funktion, welche den Ersatzwiderstand der Reihenschaltung aus r1 und r2 berechnet
     * -> 3. Aufgabe
     */
    public static double reihenschaltung(double r1, double r2) {
	double rGes = r1 + r2;
	return rGes;
    }
    
    /*Eine Funktion, welche den Ersatzwiderstand der Parallelschaltung aus r1 und r2 berechnet
     * -> 4. Aufgabe
     */
    public static double parallelschaltung(double r1, double r2) {
	double rGes = r1 * r2 / (r1 + r2);
	return rGes;
    }

}
