/**
 * Um die Quadratzahl x² zu berechnen
 * 
 * @author Simon Asmus
 * @version 1.0
 */
public class Quadrieren {
    
    public static void main(String[] args) {
	// (E) "Eingabe"
	// Wert für x festlegen:
	// ===========================
	System.out.println("Dieses Programm berechnet die Quadratzahl x²");
	System.out.println("---------------------------------------------");
	double x = 5;
				
	// (V) Verarbeitung
	// Mittelwert von x und y berechnen:
	// ================================
	double ergebnis= quadriere(x);

	// (A) Ausgabe
	// Ergebnis auf der Konsole ausgeben:
	// =================================
	System.out.printf("x = %.2f und x²= %.2f\n", x, ergebnis);
    }	
    
    public static double quadriere(double y) {
	double z = y * y;
	return z;
    }
}

