import java.util.Scanner;

/**
 * A2.2: AB "Konsoleneingabe"
 * Konsoleneingabe 2
 * 
 * @author Simon Asmus
 * @version 1.2 2021
 */
public class Konsoleneingabe2 {

    public static void main(String[] args) {
	
	Scanner myScanner = new Scanner (System.in);
	System.out.println("Guten Tag!");
	
	System.out.print("Bitte geben Sie Ihren Namen ein: ");
	String name = myScanner.nextLine();
	
	System.out.print("Bitte geben Sie Ihr Alter ein: ");
	String alter = myScanner.nextLine();
	
	System.out.print("\n\nName: " + name + ", Alter: " + alter);
	
	myScanner.close();

    }

}
