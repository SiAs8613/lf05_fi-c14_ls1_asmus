import java.util.Scanner;

/**
 * A2.2: AB "Konsoleneingabe"
 * Rechner
 * 
 * @author Simon Asmus
 * @version 1.4 2021
 */
public class Rechner {

    public static void main(String[] args) {
	
	Scanner myScanner = new Scanner (System.in);
	System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
	int zahl1 = myScanner.nextInt();
	System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
	int zahl2 = myScanner.nextInt();
	int summe = zahl1 + zahl2;
	System.out.print("\nErgebnis der Addition lautet: ");
	System.out.print(zahl1 + " + " + zahl2 + " = " + summe);
	
	System.out.print("\n\n\n\nBitte geben Sie eine ganze Zahl ein: ");
	int zahl3 = myScanner.nextInt();
	System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
	int zahl4 = myScanner.nextInt();
	int differenz = zahl3 - zahl4;
	System.out.print("\nErgebnis der Subtraktion lautet: ");
	System.out.print(zahl3 + " - " + zahl4 + " = " + differenz);
	
	System.out.print("\n\n\n\nBitte geben Sie eine ganze Zahl ein: ");
	int zahl5 = myScanner.nextInt();
	System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
	int zahl6 = myScanner.nextInt();
	int produkt = zahl5 * zahl6;
	System.out.print("\nErgebnis der Multiplikation lautet: ");
	System.out.print(zahl5 + " * " + zahl6 + " = " + produkt);
	
	System.out.print("\n\n\n\nBitte geben Sie eine ganze Zahl ein: ");
	int zahl7 = myScanner.nextInt();
	System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
	int zahl8 = myScanner.nextInt();
	int quotient = zahl7 / zahl8;
	System.out.print("\nErgebnis der Division lautet: ");
	System.out.print(zahl7 + " / " + zahl8 + " = " + quotient);
	
	System.out.print("\n\n\n\nBitte geben Sie eine ganze Zahl ein: ");
	int zahl9 = myScanner.nextInt();
	System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
	int zahl10 = myScanner.nextInt();
	int modulo = zahl9 % zahl10;
	System.out.print("\nRest der Division lautet: ");
	System.out.print(zahl9 + " % " + zahl10 + " = " + modulo);
	
	myScanner.close();

    }

}
