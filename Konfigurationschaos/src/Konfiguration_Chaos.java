/**
 * A2.1 Reparatur Konfigurationsdatei Fahrkartenautomat
 * 
 * @author Simon Asmus
 * @version 1.3 2021
 */
public class Konfiguration_Chaos {

    public static void main(String[] args) {
	
	double maximum = 100.00;
	double patrone = 46.24;
	double fuellstand;
	final byte PRUEFNR = 4;
	char sprachModul = 'd';
	String typ = "Automat AVR";
	String bezeichnung = "Q2021_FAB_A";
	String name;
	int muenzenCent = 1280;
	int muenzenEuro = 130;
	int summe;
	int euro;
	int cent;
	boolean statusCheck;
	fuellstand = maximum - patrone;
	name = typ + " " + bezeichnung;
	summe = muenzenCent + muenzenEuro * 100;
	euro = summe / 100;
	cent = summe % 100;
	statusCheck = (euro <= 150) && (euro >= 50) && (cent != 0) && (fuellstand >= 50.00) && (sprachModul == 'd') &&  (!(PRUEFNR == 5 || PRUEFNR == 6));
	
	System.out.println("Name: " + name);
	System.out.println("Sprache: " + sprachModul);
	System.out.println("Prüfnummer: " + PRUEFNR);
	System.out.println("Füllstand Patrone: " + fuellstand + " %");
	System.out.println("Summe Euro: " + euro +  " Euro");
	System.out.println("Summe Rest: " + cent +  " Cent");		
	System.out.println("Status: " + statusCheck);
	
    }
    
}
