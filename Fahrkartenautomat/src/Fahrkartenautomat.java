﻿import java.util.Scanner;

/**
 * LS05.1: Fahrkartenautomat
 * 
 * @author Simon Asmus
 * @version 6.9 2022
 */

class Fahrkartenautomat {
    
    public static void main(String[] args) {
	
	for (int i = 0; i < 1; i++) {
	    
	    	System.out.println("Guten Tag!");
        	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        	double eingezGesBetr = fahrkartenBezahlen(zuZahlenderBetrag);
        	fahrkartenAusgeben();
        	System.out.println("\n");
        	rueckgeldAusgeben(zuZahlenderBetrag, eingezGesBetr);
        	
        	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                        "vor Fahrtantritt entwerten zu lassen!\n"+
                        "Wir wünschen Ihnen eine gute Fahrt.");
        	System.out.println("\n");
        	System.out.println("-------------------------------------------"
        		+ "--------------------------------------------");
        	System.out.println("\n");
        	System.out.println("\n");
                i--;  
	}
    }
            
            /* Erfassen der Fahrkartenbestellung
             * 
             */
            public static double fahrkartenbestellungErfassen() {
        	Scanner tastatur = new Scanner(System.in);
        	String[] tickets = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
        		"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB",
        		"Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB",
        		"Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        	double[] preise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
        	int eingabe = 0;
        	int ticketWahl = 0;
        	int anzTickets = 0;
        	int zaehler1 = 0;
        	int zaehler2 = 0;
        	double preisProTicket = 0;
        	double zwischenergebnis = 0;
        	double zuZahlen;
        	do {
            	    System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus: ");
            	    for (int i = 0; i < 10; i++) {
            		String s = tickets[i];
            		double d = preise[i];
            		System.out.printf("%-35s", s);
            		System.out.printf("%6.2f EURO\n", d);
            	    }
            	    System.out.println("(0) Bezahlen");
            	    do {
            	        System.out.print("Ihre Wahl: ");
            	        eingabe = tastatur.nextInt();
            	        if (eingabe == 1 || eingabe == 2 || eingabe == 3 || eingabe == 4 || 
            	        	eingabe == 5 || eingabe == 6 || eingabe == 7 || eingabe == 8 || 
            	        	eingabe == 9 || eingabe == 10) {
            	            ticketWahl = eingabe;
            	            zaehler1++;
            	        } else if (eingabe == 0) {
            	            zaehler1++;
            	        } else {
            	            System.out.println(">> Falsche Eingabe <<");
            	            System.out.println("Bitte versuchen Sie es erneut.");
            	        }  
            	    } while (zaehler1 == 0);      	    
            	    while (zaehler2 == 0) {
            		System.out.print("\nBitte geben Sie die Anzahl der Tickets ein, die sie kaufen möchten: ");
            		anzTickets = tastatur.nextInt();
            		if (anzTickets == 1 || anzTickets == 2 || anzTickets == 3 || anzTickets == 4 || anzTickets == 5 
            			|| anzTickets == 6 || anzTickets == 7 || anzTickets == 8 || anzTickets == 9 || anzTickets == 10 
            			|| anzTickets == 0) {
            		    zaehler2++;
            		} else {
            		    System.out.println("Sie haben einen ungültigen Wert eingegeben. Sie müssen eine ganze Zahl "
            			    + "zwischen 1 und 10 für die Anzahl der Tickets eingeben.");
            		} 
            	    }
            	    if (eingabe == 0) {
            		zwischenergebnis = zwischenergebnis;
            	    } else {
            		zwischenergebnis = zwischenergebnis + anzTickets * preise[ticketWahl-1];
            	    }
        	} while (eingabe != 0);
           	
        	//if (ticketWahl == 1) {
        	  //  preisProTicket = 3.00;
        	//}
        	//if (ticketWahl == 2) {
        	  //  preisProTicket = 8.80;
        	//}
        	//if (ticketWahl == 3) {
        	  //  preisProTicket = 25.50;
        	//}
        	//zuZahlen = anzTickets * preise[ticketWahl-1];
           	zuZahlen = zwischenergebnis;
        	return zuZahlen;
            }
            
            /* Bezahlung der Fahrkarten
             * 
             */
            public static double fahrkartenBezahlen(double zuZahlen) {
        	Scanner tastatur = new Scanner(System.in);
        	double eingezahlterGesamtbetrag;
        	double eingeworfeneMünze;	
        	 // Geldeinwurf
        	 // -----------
        	 eingezahlterGesamtbetrag = 0.0;
        	 while(eingezahlterGesamtbetrag < zuZahlen) {
        	     System.out.print("Noch zu zahlen: ");
        	     System.out.printf("%.2f", (zuZahlen - eingezahlterGesamtbetrag));
        	     System.out.print(" Euro");
        	     //System.out.printf("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
        	     //System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
        	     System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
        	     eingeworfeneMünze = tastatur.nextDouble();
        	     if (eingeworfeneMünze == 0.05 || eingeworfeneMünze == 0.10 || eingeworfeneMünze == 0.20 
        		 || eingeworfeneMünze == 0.50 || eingeworfeneMünze == 1 || eingeworfeneMünze == 2) {
        		 eingezahlterGesamtbetrag += eingeworfeneMünze;  
        	     } else {
        		 System.out.println("Ungültige Eingabe.");
        	     }
        	 
        	 }
        	 return eingezahlterGesamtbetrag;
            }
            
            /* Ausgabe der Fahrkarten
             * 
             */
            public static void fahrkartenAusgeben() {
        	// Fahrscheinausgabe
        	// -----------------
        	System.out.println("\nFahrschein wird ausgegeben");
        	for (int i = 0; i < 8; i++) {
        	    System.out.print("=");
        	    warte(250);
        	}
            }
            
            /*
             * 
             */
            public static void rueckgeldAusgeben(double zuZahlen, double eingezahlterGesamtbetrag) {
        	double rückgabebetrag;
        	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
        	// Rückgeldberechnung und -Ausgabe
        	// -------------------------------
        	//rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        	if(rückgabebetrag > 0.0) {
        	    //System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
        	    //System.out.println("wird in folgenden Münzen ausgezahlt:");
        	    System.out.print("Der Rückgabebetrag in Höhe von ");
        	    System.out.printf("%.2f", rückgabebetrag);
        	    System.out.print(" EURO");
        	    System.out.println("\nwird in folgenden Münzen ausgezahlt:");
        
        	    while(rückgabebetrag >= 2.0) // 2 EURO-Münzen 
        	    {
        		muenzeAusgeben(2, "EURO");
        		rückgabebetrag -= 2.0;
        	    }
        	    while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        	    {
        		muenzeAusgeben(1, "EURO");
        		rückgabebetrag -= 1.0;
        	    }
        	    while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        	    {
        		muenzeAusgeben(50, "CENT");
        		rückgabebetrag -= 0.5;
        	    }
        	    while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        	    {
        		muenzeAusgeben(20, "CENT");
        	 	rückgabebetrag -= 0.2;
        	    }
        	    while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        	    {
        		muenzeAusgeben(10, "CENT");
        		rückgabebetrag -= 0.1;
        	    }
        	    while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        	    {
        		muenzeAusgeben(5, "CENT");
        	 	rückgabebetrag -= 0.05;
        	    }
        	}
            }
            
            /* Warten für eine bestimmte Zeit
             * 
             */
            public static void warte (int millisekunde) {
        	try {
        	    Thread.sleep(millisekunde);
        	} catch (InterruptedException e) {
        	    // TODO Auto-generated catch block
        	    e.printStackTrace();
        	}
            }
            
            /* Ausgeben einer Münze
             * 
             */
            public static void muenzeAusgeben(int betrag, String einheit) {
        	System.out.printf("\n%s", "    ****   ");
        	System.out.printf("\n%s", "  *      * ");
        	System.out.print("\n*    " + betrag + "     *");
        	System.out.print("\n*   " + einheit + "   *");
        	System.out.printf("\n%s", "  *      * ");
        	System.out.printf("\n%s", "    ****   ");
        	System.out.println(" ");
        	
            }
    
}