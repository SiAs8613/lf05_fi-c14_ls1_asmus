import java.util.Scanner;
/** LS05.1: Fahrkartenautomat - A4.1: Einführung Auswahlstrukturen
 *  AB - Auswahlstrukturen 
 *  Aufgabe 1: Eigene Bedingungen 
 *  
 * @author Simon Asmus
 * @version 1.4
 *
 */
public class Verzweigungen {

    /**
     * 
     */
    public static void main(String[] args) {
	Scanner eingabe = new Scanner(System.in);
	System.out.println(" Bitte geben Sie 3 ganze Zahlen ein.");
	System.out.println(" Erste Zahl: ");
	int ersteZahl = eingabe.nextInt();
	System.out.println(" Zweite Zahl: ");
	int zweiteZahl = eingabe.nextInt();
	System.out.println(" Dritte Zahl: ");
	int dritteZahl = eingabe.nextInt();
	
	if (ersteZahl > zweiteZahl && ersteZahl > dritteZahl) {
	    System.out.println(" " + ersteZahl + " ist größer als " + zweiteZahl + " und größer als " + dritteZahl);
	}
	
	if (dritteZahl > zweiteZahl || dritteZahl > ersteZahl) {
	    System.out.println(" " + dritteZahl + " ist größer als " + zweiteZahl + " oder größer als " + ersteZahl);
	}
	
	if (ersteZahl > zweiteZahl && ersteZahl > dritteZahl) {
	    System.out.println(" " + ersteZahl + " ist größer als " + zweiteZahl + " und größer als " + dritteZahl);
	} else if (zweiteZahl > dritteZahl && zweiteZahl > ersteZahl) {
	    System.out.println(" " + zweiteZahl + " ist größer als " + dritteZahl + " und größer als " + ersteZahl);
	} else {
	    System.out.println(" " + dritteZahl + " ist größer als " + zweiteZahl + " und größer als " + ersteZahl);
	}
	
    }

}
